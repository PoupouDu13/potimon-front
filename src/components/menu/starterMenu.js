import React from 'react';
import { Container, Row } from 'reactstrap';
import StarterDetail from './starterDetail';

class StarterMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            starterIds: [
                1,
                4,
                7
            ]
        }

        this.handleStarterClick = this.handleStarterClick.bind(this);
    }

    handleStarterClick() {
        alert('starter !!!');
    }

    render() {

        return (
            <div className='starter-menu' >
                <Container fluid={true}>
                    <Row>
                        {
                            this.state.starterIds.map((id) => 
                                <StarterDetail key={id} id={id} portrait={this.props.portraits[`${id}.gif`]} />
                            )
                        }
                    </Row>
                </Container>
            </div>
        );
    }
}

export default StarterMenu;