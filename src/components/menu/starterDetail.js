import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import SelectPotimonApi from '../../services/selectPotimonApi';
import './css/starterDetail.css'

class StarterDetail extends React.Component {

    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const id = this.props.id;

        SelectPotimonApi.loadPotimon(id);
    }

    render() {

        return (
            <div className='starter-detail' >
                <Container fluid={true}>
                    <Row>
                        <Col sm='12'>
                            <img src={this.props.portrait} alt='woops' className='starter-height' onClick={this.handleClick} />
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default StarterDetail;