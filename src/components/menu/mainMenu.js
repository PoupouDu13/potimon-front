import React from "react";
import { Container, Button } from "reactstrap";
import LaunchFight from "../fight/launchFight";
import authenticateApi from "../../services/authenticateApi";

class MainMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      potimonsTeam: [],
      potimonsReserve: [],
      gameInfo: {},
      items: {}
    };

    this.handleTeamMenuClick = this.handleTeamMenuClick.bind(this);
  }

  handleTeamMenuClick() {
    alert("show team menu");
  }

  logout() {
    authenticateApi.logout();
    // Here route to the App component (manually or with react router).
    // In the meantime, reload page, as App.js display the correct component according to user status.
    document.location.reload();
  }

  render() {
    return (
      <div className="main-menu">
        <Container fluid={true}>
          <Button onClick={this.handleTeamMenuClick} color="primary">
            {" "}
            team info{" "}
          </Button>
          <LaunchFight buttonLabel="fight !" />
          <Button onClick={() => this.logout()} color="danger">
            Logout
          </Button>
        </Container>
      </div>
    );
  }
}

export default MainMenu;
