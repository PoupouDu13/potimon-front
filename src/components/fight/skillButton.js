import React from 'react';
import { Button } from 'reactstrap';

const SkillButton = ({ id, name, color }, handleSkillClick) => (
    <Button key={id} color={color} onClick={handleSkillClick}>{name}</Button>
);

export default SkillButton;