import React from 'react';
import Animate from '../../helpers/animate'

class AttackImage extends React.Component {

    componentDidMount() {
        Animate.animateOverHead(this.props.targetGameId, this.props.usedSkillId);   
     }

    render() {
        return (
            <div className='attack-animation'>
                <img id={this.props.id} src={this.props.attackImage} alt='woops' />
            </div>
        );
    }
}

export default AttackImage;