import React from 'react';
import ReactDOM from 'react-dom';
import { Button } from 'reactstrap';
import FightScreen from './fightScreen';
import Helper from '../../helpers/helper';
import plaine from '../../assets/images/maps/2.png';

class LaunchFight extends React.Component {

    handleClick = () => {        
        ReactDOM.render(
            <FightScreen
                image={plaine}
                potimonFrontImages={Helper.loadFrontPotimonImage()}
                potimonBackImages={Helper.loadBackPotimonImage()}
                skillImages={Helper.loadSkillsImage()}
            />,
            document.getElementById('root'));
    }

    render() {
        return (
            <Button onClick={this.handleClick} > {this.props.buttonLabel} </Button>
        );
    }
}

export default LaunchFight;