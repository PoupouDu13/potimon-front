import React from 'react';
import { Progress, Row, Col } from 'reactstrap';

const TeamInfoDetail = ({ gameId, hp, maxHp, mana, maxMana, level, name }) => (
    <Row key = {gameId}>
        <Col sm='4'>
            {name}
        </Col>
        <Col sm='4'>
            <Progress value={hp / maxHp * 100}>{hp}\{maxHp}</Progress>
            <Progress value={mana / maxMana * 100}>{mana}\{maxMana}</Progress>
        </Col>        
        <Col sm='2'>
            {level}
        </Col>
    </Row>  
);

export default TeamInfoDetail;