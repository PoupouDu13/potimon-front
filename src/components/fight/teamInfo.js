import React from 'react';
import { Container } from 'reactstrap';
import TeamInfoDetail from './teamInfoDetail';

class TeamInfo extends React.Component {

    render() {
        return (
            <Container fluid={true}>
                {this.props.potimons.map(potimon => TeamInfoDetail(potimon))}
            </Container>
        );
    }
}

export default TeamInfo;