import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import TeamInfo from './teamInfo';
import SkillNavBar from './skillNavBar';
import PotimonImage from './potimonImage';
import AttackImage from './attackImage';
import './css/fightScreen.css';

class FightScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            bottomPotimons: [
                {
                    id: 1,
                    gameId: 'azeaz-g135',
                    name: 'potizar',
                    hp: 10,
                    maxHp: 10,
                    mana: 10,
                    maxMana: 10,
                    level: 12,
                    skills: [
                        { id: 10, name: 'claw', manaCost: 0 },
                        { id: 45, name: 'growl', manaCost: 0 },
                    ]
                },
                {
                    id: 4,
                    gameId: 'zele-4321',
                    name: 'potimeche',
                    hp: 8,
                    maxHp: 12,
                    mana: 4,
                    maxMana: 6,
                    level: 10,
                }
            ],
            topPotimons: [
                {
                    id: 1,
                    gameId: 'ekgrje-4687',
                    name: 'potizar',
                    hp: 10,
                    maxHp: 10,
                    mana: 10,
                    maxMana: 10,
                    level: 12,
                },
                {
                    id: 4,
                    gameId: 'pozu-1375',
                    name: 'potimeche',
                    hp: 8,
                    maxHp: 12,
                    mana: 4,
                    maxMana: 6,
                    level: 10,
                }
            ],
            currentPotimonGameId: 'azeaz-g135',
            showNavBar: true,
            humanIsChosingTarget: false,
            targetGameId: '',
            usedSkill: '',
            showAttackImage: false,            
        };

        this.handleMouseEnter = this.handleMouseEnter.bind(this);
        this.handleSkillClick = this.handleSkillClick.bind(this);
        this.handleContextMenu = this.handleContextMenu.bind(this);
        this.handleTargetClick = this.handleTargetClick.bind(this);
    }

    getPotimon(gameId) {
        return this.state.topPotimons.concat(this.state.bottomPotimons).find(potimon => potimon.gameId === gameId);
    }

    handleSkillClick(skill) {
        document.getElementById("root").style.cursor = "crosshair";
        this.setState({
            showNavBar: false,
            humanIsChosingTarget: true,
            usedSkill: skill,
        });
    }

    handleContextMenu(e) {
        e.preventDefault();
        document.getElementById("root").style.cursor = "auto";
        this.setState({
            humanIsChosingTarget: false,
            targetGameId: '',
        });
    }

    handleMouseEnter(e) {
        this.setState({ targetGameId: e.currentTarget.id });
    }

    handleTargetClick(e) {
        this.setState({
            showAttackImage: true,
            humanIsChosingTarget: false,
        });
        document.getElementById("root").style.cursor = "auto";
        setTimeout(() => {
            this.setState({
                showAttackImage: false,
            })
        }, 1000)
    }

    render() {

        const divStyle = {
            backgroundImage: 'url(' + this.props.image + ')'
        }

        return (
            <div className='back-screen' style={divStyle} onContextMenu={(this.state.humanIsChosingTarget) ? this.handleContextMenu : null}>
                <Container fluid={true}>
                    <Row>
                        <Col sm='4'>
                            <TeamInfo potimons={this.state.topPotimons} />
                        </Col>
                        <Col sm='6' className='potimon-normal-height'>
                            <Row>
                                {
                                    this.state.topPotimons.map(
                                        (potimon) =>
                                            <Col sm={12 / this.state.topPotimons.length} onMouseEnter={(this.state.humanIsChosingTarget) ? this.handleMouseEnter : null}
                                                key={potimon.gameId} id={potimon.gameId} onClick={(this.state.targetGameId === potimon.gameId) ? this.handleTargetClick : null}>
                                                <PotimonImage className={(this.state.targetGameId === potimon.gameId) ? 'potimon-large-image' : 'potimon-normal-image'}
                                                    imgUrl={this.props.potimonFrontImages[`${potimon.id}.gif`]} gameId={potimon.gameId} />
                                            </Col>
                                    )
                                }
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='6' className='potimon-normal-height'>
                            <Row>
                                {
                                    this.state.bottomPotimons.map(
                                        (potimon) =>
                                            <Col sm={12 / this.state.bottomPotimons.length} onMouseEnter={(this.state.humanIsChosingTarget) ? this.handleMouseEnter : null} key={potimon.gameId} id={potimon.gameId}>
                                                <PotimonImage className='potimon-normal-image' imgUrl={this.props.potimonBackImages[`${potimon.id}.gif`]} gameId={potimon.gameId} />
                                            </Col>
                                    )
                                }
                            </Row>
                        </Col>
                        <Col sm='4'>
                            <TeamInfo potimons={this.state.bottomPotimons} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='12'>
                            {
                                (this.state.showNavBar) ?
                                    <SkillNavBar
                                        potimon={this.getPotimon(this.state.currentPotimonGameId)}
                                        handleSkillClick={this.handleSkillClick}
                                    />
                                    :
                                    null
                            }
                        </Col>
                    </Row>
                    {
                        (this.state.showAttackImage) ?
                            <AttackImage
                                targetGameId={this.state.targetGameId}
                                usedSkillId={this.state.usedSkill.id}
                                attackImage={this.props.skillImages[`${this.state.usedSkill.id}.gif`]}
                                key={this.state.targetGameId + this.state.usedSkill.id}
                                id={this.state.targetGameId + this.state.usedSkill.id}
                            />
                            :
                            null
                    }
                </Container>
            </div>
        );
    }
}

export default FightScreen;