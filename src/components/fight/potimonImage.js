import React from 'react';
import './css/potimonImage.css'

const PotimonImage = ({ imgUrl, gameId, className }) => (
    <img src={imgUrl} alt={gameId} className={className} />
);

export default PotimonImage;