import React from 'react';
import { Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import authenticateApi from '../../services/authenticateApi';

export default class LoginForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      error: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const usernameInput = this.state.username;
    const passwordInput = this.state.password;

    authenticateApi.login({ username: usernameInput, password: passwordInput })
      .then((user) => {
        // Ici router vers le main menu (en custom avec renderDom ou avec react-router)
        // En attendant je reload la page, car App.js load le bon component suivant si on est loggé ou pas
        document.location.reload();
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          error: err.response ? err.response.data.message : err.message
        })
      })
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit} >
        {this.state.error && (
          <Alert color="danger">{this.state.error}</Alert>
        )}
        <FormGroup>
          <Label for="userName">User name</Label>
          <Input type="text" name="username" id="userNameNewUserForm" placeholder="example: potiron" onChange={this.handleInputChange} />
        </FormGroup>
        <FormGroup>
          <Label for="password">Password</Label>
          <Input type="password" name="password" id="passwordNewUserForm" placeholder="example: potironpassword" onChange={this.handleInputChange} />
        </FormGroup>

        <Button color='danger'>Submit</Button>
      </Form>
    );
  }
}