import React from 'react';
import ReactDOM from 'react-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import authenticateApi from '../../services/authenticateApi';
import StarterMenu from '../menu/starterMenu';
import Helper from '../../helpers/helper'

export default class NewUserForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const usernameInput = this.state.username;
    const passwordInput = this.state.password;
    const emailInput = this.state.email;
    authenticateApi.subscribe({ username: usernameInput, email: emailInput, password: passwordInput }).then((response) => {
      if (response.message) {
        alert(response.message);
      } else {
        console.log(response.data);
        ReactDOM.render(
          <StarterMenu portraits={Helper.loadPortraitPotimonImage()} />,
          document.getElementById('root')
        );
      }
    });
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit} >
        <FormGroup>
          <Label for="userName">User name</Label>
          <Input type="text" name="username" id="userNameNewUserForm" placeholder="example: potiron" onChange={this.handleInputChange} required />
        </FormGroup>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input type="email" name="email" id="emailNewUserForm" placeholder="example: potiron@gmail.com" onChange={this.handleInputChange} required />
        </FormGroup>
        <FormGroup>
          <Label for="password">Password</Label>
          <Input type="password" name="password" id="passwordNewUserForm" placeholder="example: potironpassword" onChange={this.handleInputChange} required />
        </FormGroup>

        <Button color='danger'>Submit</Button>
      </Form>
    );
  }
}