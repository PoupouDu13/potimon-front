import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL || 'http://localhost:4000/api/selectPotimon'

const loadPotimon = async (id) => {
    axios.all([
        axios.get(`${API_URL}/baseExperience`, { params: { id: id } }),
        axios.get(`${API_URL}/stats`, { params: { id: id } }),
        axios.get(`${API_URL}/moves`, { params: { id: id } }),
        axios.get(`${API_URL}/types`, { params: { id: id } }),
        axios.get(`${API_URL}/evolution`, { params: { id: id } }),
        axios.get(`${API_URL}/captureRate`, { params: { id: id } }),
    ]).then(axios.spread((a1, a2, a3, a4, a5, a6) => {
        return formatPotimonData(a1, a2, a3, a4, a5, a6);
    }));
}

const formatPotimonData = (a1, a2, a3, a4, a5, a6) => {
    const result = {};
    result.baseExperience = a1.data[0][0].base_experience;
    result.stats = a2.data[0][0];
    result.moves = a3.data[0][0];
    result.types = a4.data[0][0];
    result.evolution = a5.data[0][0];
    result.captureRate = a6.data[0][0];

    return result
}

export default {
    loadPotimon,
}