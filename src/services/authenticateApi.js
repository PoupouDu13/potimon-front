import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL || 'http://localhost:4000/api/user'

axios.interceptors.request.use((config) => {
    const token = localStorage.getItem('token');
    if (token) {
        config.headers.Authorization = 'Bearer ' + token;
    }

    return config;
})

axios.interceptors.response.use((response) => {
    if (response.status === 401) {
        console.log('401');

    }
    return response;
})

const subscribe = async ({ username, email, password }) => {
    try {
        const response = await axios.post(`${API_URL}/insertUser`,
            {
                username: username,
                email: email,
                password: password,
            }
        );
        return response;
    } catch (error) {
        return {
            message: 'Subscription failed'
        }
    }

}

const login = async ({ username, password }) => {
    const { data } = await axios.post(`${API_URL}/login`,
        {
            username: username,
            password: password,
        }
    );

    localStorage.setItem('token', data.token);
    return data;
}

const logout = () => {
    localStorage.removeItem('token');
}

// const login = async ({ email, password }) => {
//     try {
//         const user = await axios.get('/login')
//         localStorage.setItem('guid', user.guid);

//         return user;
//     } catch (e) {
//         return {
//             message: 'Login failed'
//         }
//     }
// }

export default {
    subscribe,
    login,
    logout
}