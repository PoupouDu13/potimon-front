import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Helper from './helpers/helper';
import 'bootstrap/dist/css/bootstrap.min.css';

const paintings = Helper.importAll(require.context(`./assets/images/painting`, false, /\.(gif|png|jpe?g|svg)$/));
const painting = paintings[`${Helper.entierAleatoire(1, 11)}.png`]

ReactDOM.render(<App backgroundImage={painting} />, document.getElementById('root'));
registerServiceWorker();
