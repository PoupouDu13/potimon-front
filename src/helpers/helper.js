const Helper = {};

Helper.entierAleatoire = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

Helper.overlapping = (node1, node2) => {
    const rect1 = node1[0].getBoundingClientRect();
    const rect2 = node2[0].getBoundingClientRect();
    return !(rect1.right < rect2.left ||
        rect1.left > rect2.right ||
        rect1.bottom < rect2.top ||
        rect1.top > rect2.bottom)
}

Helper.guidGenerator = () => {
    const S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

Helper.max = (int1, int2) => {
    return (int1 > int2) ? int1 : int2;
}

Helper.min = (int1, int2) => {
    return (int1 < int2) ? int1 : int2;
}

Helper.getSum = (total, num) => {
    return total + num;
}

Helper.importAll = (r) => {
    let images = {};
    r.keys().map((item, index) => {
        images[item.replace('./', '')] = r(item);
    });
    return images;
}

Helper.loadFrontPotimonImage = () => {
    console.log('load front images');    
    return Helper.importAll(require.context(`../assets/images/potimons/front`, false, /\.(gif|png|jpe?g|svg)$/));
}

Helper.loadBackPotimonImage = () => {
    return Helper.importAll(require.context(`../assets/images/potimons/back`, false, /\.(gif|png|jpe?g|svg)$/));
}

Helper.loadSkillsImage = () => {
    return Helper.importAll(require.context(`../assets/images/skills`, false, /\.(gif|png|jpe?g|svg)$/));
}

Helper.loadPortraitPotimonImage = () => {
    return Helper.importAll(require.context(`../assets/images/potimons/portrait`, false, /\.(gif|png|jpe?g|svg)$/));
}

export default Helper;
