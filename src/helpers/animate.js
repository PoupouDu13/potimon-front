const $ = require("jquery");

const Animate = {};

Animate.deleteBody = () => {
    $.each($('body').children(), (index, child) => {
        if (child.id !== 'root') {
            child.remove();
        }
    });
}

Animate.animateOverHead = (targetId, skillId) => {
    if (targetId && skillId) {
        const targetElement = $(`#${targetId}`);
        const gifOverHead = $(`#${targetId + skillId}`);
        gifOverHead.css({
            position: 'absolute',
            zIndex: '10',
            height: targetElement.height() + 'px',
            width: targetElement.height() + 'px',
        });        
        setTimeout(function () {
            gifOverHead.css({
                position: 'absolute',
                left: targetElement.offset().left + 'px',
                top: targetElement.offset().top + 'px',
            });
        }, 10);    
    }    
}


export default Animate;
