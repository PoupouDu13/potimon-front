import React, { Component } from 'react';
import NewUserModal from './components/authenticate/newUserModal';
import LoginModal from './components/authenticate/loginModal';
import MainMenu from './components/menu/mainMenu';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    const token = localStorage.getItem('token');

    this.state = {
      token
    }
  }

  render() {
    var divStyle = {
      backgroundImage: 'url(' + this.props.backgroundImage + ')'
    }

    return (
      <div className="App" style={divStyle}>
        <header className="App-header">
          <h1 className="App-title">Welcome to Potimon</h1>
        </header>

        {this.state.token ? <MainMenu/> : (
          <div>
            <NewUserModal buttonLabel='new user' />
            <LoginModal buttonLabel='log in' />
          </div>
        )}
      </div>
    );
  }
}

export default App;
